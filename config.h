#define FORCE_VSPLIT  0
#define Alt Mod1Mask
#define Super Mod4Mask
#define Cap Mod3Mask
#define Shift ShiftMask
#define Mouse Mod4Mask|Mod3Mask
#define Mouses Mod4Mask|Mod3Mask|Shift
#define TAGKEYS(KEY,TAG) \
	{ Super, -1, KEY, view, {.ui = 1 << TAG} }, \
	{ Super|Cap, -1, KEY, toggleview, {.ui = 1 << TAG} }, \
	{ Super|Cap|Shift, -1, KEY, toggletag, {.ui = 1 << TAG} }, \
	{ Super|Shift, -1, KEY, tag, {.ui = 1 << TAG} }, \
	{ Alt|Shift, -1, KEY, tagunfocused, {.ui = 1 << TAG} }, \
	{ Alt, -1, KEY, swaptags, {.ui = 1 << TAG} },\
	{ Super|Alt, -1, KEY, tagnextmon, {.ui = 1 << TAG} }, \
	{ Super|ControlMask, -1, KEY, tagprevmon, {.ui = 1 << TAG} },

				//1      2       3        4       5     6       7        8      9      10    11     12    13     14     15    16      17     18    19     20      21     22
static const unsigned int stairpx   = 20;       /* depth of the stairs layout */
static const int stairsamesize      = 1;        /* 1 means shrink all the staired windows to the same size */
static const int barh      = 35;
static const int stmo      = 2;
static const char *tags[] =  { "  ", "", "  ", "  ", "  ","  ", "  ", "  ", "  "," ","  "," ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  " };
static const char *tagsalt[] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"};
static const char *layoutmenu_cmd = "dwm-layout";
static const float toofact     = 0.45; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int decorhints = 1;
static const int statuslong = 290;       /* vertical padding of bar */
static const int vertpad = 0;       /* vertical padding of bar */
static int sidepad = 0;       /* horizontal padding of bar */
static const unsigned int gappx = 5;        /* gaps between windows */
static const unsigned int borderpx = 0;        /* border pixel of windows */
static const unsigned int snap = 32;       /* snap pixel */
static const int showbar = 1;        /* 0 means no bar */
static const int baroff = 10;        /* 0 means no bar */
static int topbar = 0;        /* 0 means bottom bar */
static int hidetag = 1;        
static const char bg[]       = "#181B28";
static const char useless[]       = "#000000";
static int attachdirection = 3; 
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termfcmd[]  = { "termite","-e","fish",NULL };
static const char *chromium[]  = { "chromium","--no-sandbox","--disable-features=ExtensionsToolbarMenu",NULL };
static const char *btw_i_use_surf_in_tabbed[]  = { "surf-tabbed",NULL };
static const char *btw_i_use_surf[]  = { "surf",NULL };
static const char *termbcmd[]  = { "termite","-e","bash",NULL };
static const char *termzcmd[]  = { "termite","-e","zsh",NULL };
static const int stairdirection     = 1; 
/* commands spawned when clicking statusbar, the mouse button pressed is exported as BUTTON */
static char *statuscmds[] = { "weat","vol","time" };
static char *statuscmd[] = { "dwm-mouse", lastbutton, NULL, NULL, };
static const char broken[] = " ";

#include <X11/XF86keysym.h>
#include "layout.c"
#include "Shiftview.c"

static const char *const autostart[] = {
	"stalonetray",NULL,
	"killall","-s9","deadd-notification-center", NULL,
	"wallshuffler",NULL,
	"xfce4-panel" , NULL,
	"picom", NULL,
	"dwm-ime" , NULL,
	"xmodmap",kcon,NULL,
	"xrdb",xcon,NULL,
	"/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1",NULL,
	"copyq",NULL,
	"xfce4-power-manager",NULL,
	"flameshot" , NULL,
	"startnextcloud",NULL,
	"deadd-notification-center", NULL,
	"yakuake", NULL,
	NULL
};

static const char *fonts[]          = { 
	"FiraCode Nerd Font:style=Light:size=10",//English
	"JoyPixels:style=Regular:size=10",//Emoji 
	"Chiron Sans HK Light:style=Light:size=10"//Chinese,Japanese,Korean
};

static const char *colors[][3]      = {
        /*               fg         bg         border   */
	[SchemeNorm] = { useless,  "#50fa7b" , bg },
	[SchemeSel] = { useless,  "#ff5555" , bg },

	[SchemeFNorm] = { useless,  "#ff79c6" , bg },
	[SchemeFSel] = { useless,   "#bd93f9" ,bg  },

	[SchemeStatus] = {   "#ff79c6",  "#ff79c6",  useless  }, // Statusbar right {text,background,not used but cannot be empty}
        [SchemeTagsSel] = {"#ff5555", bg,   useless  }, // Tagbar left selected {text,background,not used but cannot be empty}
	
	[SchemeTagsNorm] = { "#bd93f9" ,bg, useless  }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel] = { "#50fa7b",bg , useless  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm] = { "#50fa7b" , bg,  useless  }, // infobar middle  selected {text,background,not used but cannot be empty}
};
//*class *title *instance *role tags isfloating isdock neverkill iscentered dontmanage issticky dontunfloating monitor
static const Rule rules[] = {	
	{ "stalonetray", "stalonetray",  NULL,NULL,  0, 1, 0, 1, 0, 0, 1, 0, -1},     
	{ "svkbd", NULL, NULL, NULL, TAGMASK, 1, 1, 0, 0, 0, 0, 0, -1},
	{ "copyq", NULL, NULL, NULL, 0, 1, 0, 0, 0, 0 ,0, 0, -1},
	{ "Abrowser", NULL, "Toolkit", "PictureInPicture", 0, 1, 0, 0, 0, 0, 1, 0, -1},
	{ "yakuake",NULL,"yakuake", NULL, 0, 1, 0, 0, 0, 0, 0, 1, -1},
	//desktop
	{ "Nemo-desktop","nemo-desktop",NULL, NULL, 0, 0, 0, 0, 0, 2, 0, 0, -1},
	{ "GLava", NULL, NULL, 0, 0, 0, 0, 0, 2, 0, 0, -1},
	{ "Nemo-desktop","Desktop",NULL, NULL, 0, 0, 0, 0, 0, 2, 0, 0, -1},
	{ "plasmashell", NULL, NULL, NULL, 0, 0, 0, 0, 0, 2, 0, 0, -1},
	{ "ffplay", NULL, NULL, NULL, 0, 0, 0, 0, 0, 2,0, 0, -1},
	{ "Conky", NULL, NULL, NULL, 0, 0, 0, 0, 0, 2,0, 0, -1},
	//panel
	{ "Xfce4-panel","xfce4-panel",NULL, NULL, 0, 0, 0, 0, 0, 3, 0, 0, -1},
	{ "lxpanel", NULL,NULL, NULL, 0, 0, 0, 0, 0, 3, 0, 0, -1},
	{ "Plank", "plank",NULL, NULL, 0, 0, 0, 0, 0, 3, 0, 0, -1},
	{ "Mate-panel", NULL,NULL, NULL, 0, 0, 0, 0, 0, 3,0, 0, -1},
	{ "Gnome-panel", NULL,NULL, NULL, 0, 0, 0, 0, 0, 3,0, 0, -1},

};

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "  ",      tatami }, //0
	{ "  ",      tile },  //1
	{ "  ",      ltile }, //2   
	{ "  ",      NULL }, //3
	{ "  ",      grid },  //4
        { "  ",      deck }, //5
	{ " T ",      bstack }, //6
        { "  ",      bstackhoriz }, //7
	{ "  ",      spiral }, //8
	{ "  ",     dwindle }, //9
	{ "  ",      centeredmaster }, //10
	{ "  ",      centeredfloatingmaster }, //11
	{ "  ",       horizgrid }, //12
	{ "  ",      gaplessgrid}, //13
	{ "  ",      onevline }, //14
	{ "  ",      onehline }, //15
	{ "  ",      nrowgrid }, //16
	{ "  ",       doubledeck }, //17
	{ "  ",      monocle }, //18
	{ " 🬮 ",      stairs }, //19
	{ NULL,       NULL },


};

static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol, 0, Button1, cyclelayout, {.i = +1 } }, 
	{ ClkLtSymbol, 0, Button3, cyclelayout, {.i = -1 } }, 
	{ ClkLtSymbol, 0, Button4, cyclelayout, {.i = +1 } },
	{ ClkLtSymbol, 0, Button5, cyclelayout, {.i = -1 } },
	
	{ ClkLtSymbol, Shift, Button1, uncyclelayout, {.i = +1 } }, 
	{ ClkLtSymbol, Shift, Button3, uncyclelayout, {.i = -1 } }, 
	{ ClkLtSymbol, Shift, Button4, uncyclelayout, {.i = +1 } },
	{ ClkLtSymbol, Shift, Button5, uncyclelayout, {.i = -1 } },
	
	{ ClkLtSymbol, Super, Button1, layoutmenu, {0} },
	{ ClkLtSymbol, Super, Button2, layoutmenu, {0} },
	{ ClkLtSymbol, Super, Button3, layoutmenu, {0} },
	{ ClkLtSymbol, Super, Button4, layoutmenu, {0} },
	{ ClkLtSymbol, Super, Button5, layoutmenu, {0} },
	
	{ ClkWinTitle, 0, Button3, movemouse, {0} },	
	{ ClkWinTitle, 0, Button1, spawn, SHCMD("playerctl --player=%any play-pause") },	
	
	{ ClkClientWin, Super, Button1, movemouse, {0} },
	{ ClkClientWin, Cap, Button3, dragmfact, {0} },
	{ ClkClientWin, Cap|Shift, Button3, dragcfact, {0} },
	{ ClkClientWin, Super, Button3, resizemouse, {0} },
	
	{ ClkTagBar, 0, Button1, view, {0} },
	{ ClkTagBar, 0, Button3, toggleview, {0} },
	{ ClkTagBar, Super, Button1, tag, {0} },	
	{ ClkTagBar, Super, Button3, toggletag, {0} },

	{ ClkStatusText, 0, Button1, spawn, {.v = statuscmd } },
	{ ClkStatusText, 0, Button2, spawn, {.v = statuscmd } },
	{ ClkStatusText, 0, Button3, spawn, {.v = statuscmd } },
	{ ClkStatusText, 0, Button4, spawn, {.v = statuscmd } },
	{ ClkStatusText, 0, Button5, spawn, {.v = statuscmd } },

	{ ClkRootWin, 0, Button3, spawn, SHCMD("xcontext") },	
};


static Key keys[] = {
	/* modifier  key  function  argument */
	{ 0, -1, XF86XK_PowerOff, spawn, SHCMD( "killall -s9 svkbd-exkc || svkbd-exkc" ) },
	{ 0, XF86XK_AudioLowerVolume, XF86XK_PowerOff, spawn, SHCMD( "slock" ) },
	
	{ 0, -1, XF86XK_AudioPrev, spawn, SHCMD( "playerctl --player=%any previous" ) },
	{ 0, -1, XF86XK_AudioNext, spawn, SHCMD( "playerctl --player=%any next" ) },
	{ 0, -1, XF86XK_AudioPlay, spawn, SHCMD( "playerctl --player=%any play-pause" ) },
	{ 0, -1, XF86XK_AudioPause, spawn, SHCMD( "playerctl --player=%any play-pause" ) },
	{ 0, -1, XF86XK_AudioStop, spawn, SHCMD( "playerctl --player=%any stop" ) },

	{ 0, -1, XF86XK_AudioRaiseVolume, spawn, SHCMD( "dwm-setvol +") },
	{ 0, -1, XF86XK_AudioLowerVolume, spawn, SHCMD( "dwm-setvol -") },
	{ 0, -1, XF86XK_AudioMute, spawn, SHCMD( "dwm-setvol m" ) },

	{ 0, XF86XK_AudioRaiseVolume, XF86XK_PowerOff, spawn, SHCMD( "dwm-setvol m" ) },
	{ 0, XF86XK_PowerOff, XF86XK_AudioRaiseVolume, spawn, SHCMD( "dwm-setvol m" ) },
	
	{ 0, -1, XK_Print, spawn, SHCMD( "flameshot gui" ) },
	{ 0, XF86XK_PowerOff, XF86XK_AudioLowerVolume, spawn, SHCMD( "slock" ) },
	{ 0, XF86XK_PowerOff, XF86XK_AudioLowerVolume, spawn, SHCMD( "slock" ) },

	{ Shift, -1, XK_Control_R, spawn, SHCMD( "killall -s9  qalculate-gtk||qalculate-gtk" ) },
	{ Shift, -1, XK_Meta_L, spawn, SHCMD( "qdbus org.kde.yakuake  /yakuake/window org.kde.yakuake.toggleWindowState" ) },
	 
	{ Alt, -1, XK_q, spawn, {.v = dmenucmd } },
	{ Alt, -1, XK_Return, spawn, {.v = termfcmd } },
	{ Alt, XK_b, XK_Return, spawn, {.v = termbcmd } },
	{ Alt, XK_z, XK_Return, spawn, {.v = termzcmd} },
	{ Alt|Shift, -1, XK_y, spawn, {.v = btw_i_use_surf_in_tabbed } },
	{ Alt, -1, XK_y, spawn, {.v = btw_i_use_surf } },

	{ Alt, -1, XK_r, spawn, SHCMD( "gimp" ) },
	{ Alt, -1, XK_d, spawn, SHCMD( "copyq show" ) },
	{ Alt, -1, XK_g, spawn, SHCMD( "thunar" ) },
	{ Alt, -1, XK_f, spawn, SHCMD( "termite -e lf" ) },
	
	{ Alt, -1, XK_Print, spawn, SHCMD( "obs" ) },
	{ Alt, -1, XK_t, spawn, SHCMD( "virtualbox" ) },
	{ Alt, -1, XK_e, spawn, SHCMD( "firedragon" ) },
	{ Alt, -1, XK_s, spawn, SHCMD( "brave" ) },

	{ Cap, -1, XK_Insert, spawn, SHCMD(" playerctl --player=%any previous" ) },
	{ Cap, -1, XK_Home, spawn, SHCMD( "playerctl --player=%any play-pause" ) },
	{ Cap, -1, XK_Prior, spawn, SHCMD( "playerctl --player=%any next" ) },
	{ Cap, -1, XK_Delete, spawn, SHCMD( "playerctl --player=%any stop" ) },

	{ Cap|Shift, -1, XK_Insert, spawn, SHCMD( "dwm-setvol -" ) },
	{ Cap|Shift, -1, XK_Home, spawn, SHCMD( "dwm-setvol m" ) },
	{ Cap|Shift, -1, XK_Prior, spawn, SHCMD( "dwm-setvol +" ) },
	
	{ Cap|Shift, -1, XK_Delete, spawn, SHCMD( "toggle_wallshuffler" ) },
	{ Alt, -1, XK_w, spawn, SHCMD( "dmenu_emoji" ) },
	{ Alt, -1, XK_x, spawn, SHCMD( "killall -s USR1 deadd-notification-center" ) },
	{ Alt, -1, XK_a, spawn, {.v = chromium } },

	{ Super|Shift, -1, XK_z, spawn, SHCMD( "toggleres" ) },
	{ Super|Shift, -1, XK_t, spawn, SHCMD( "tabwin remove" ) },
	{ Super, -1, XK_t, spawn, SHCMD( "tabwin add" ) },
	{ Super, -1, XK_Print, spawn, SHCMD( "mkdir -p ~/Pictures/Screenshot && flameshot full -p ~/Pictures/Screenshot/" ) },
	{ Super|Shift, -1,	XK_s, spawn, SHCMD( "slock" ) },
	
	{ Super, -1, XK_q, transfer, {0} },
	{ Super|Shift, -1, XK_q, transferall, {0} },

	{ Super, -1, XK_d, oneclientpertags, {0} },	
	{ Super, -1, XK_w, unfloatvisible, {0} },	
	{ Super, -1, XK_r, reorganizetags, {0} },
	{ Super|Shift, -1, XK_grave, togglesticky, {0} },
	{ Super, -1,  XK_a, togglealwaysontop, {0} },

	{ Super, -1, XK_Tab, shiftview, { .i = -1 } },
	{ Super|Shift, -1,	XK_Tab,	shifttag, { .i = -1 } },

	{ Super, -1, XK_Page_Up, viewtoleft, {0} },
	{ Super, -1, XK_Page_Down, viewtoright, {0} },

	{ Super|Shift, -1, XK_Page_Up, tagtoleft, {0} },
	{ Super|Shift, -1, XK_Page_Down, tagtoright, {0} },

	{ Super, -1, XK_p, togglevtag, {0} },
	{ Super|Shift, -1, XK_p, toggletbar, {0} },
	{ Super, -1, XK_b, togglebar,	 	{0} },
	
	{ Super|Shift, -1,  XK_j, rotatestack,  {.i = +1 } },
	{ Super|Shift,   -1, XK_k, rotatestack,  {.i = -1 } },
	
	{ Super, -1,  XK_j, focusstack, {.i = +1 } },
	{ Super, -1,  XK_k, focusstack, {.i = -1 } },
	
	{ Super, -1,  XK_h, setmfact,  {.f = -0.05} },
	{ Super, -1,  XK_x, layoutmenu,  {0} },
	{ Super, -1,  XK_l, setmfact,  {.f = +0.05} },
	{ Super, -1,  XK_u, resetmfact,  {0} },

	{ Super,-1,  XK_Return, zoom,  {0} },
	{ Super|Shift, -1, XK_Return, focusmaster, {0} },
	{ Super, -1, XK_n, togglealttag, {0} },
	{ Super|Shift, -1, XK_n, togglegap, {0} }
	,
	{ Super, -1, XK_Tab, view, {0} },
	{ Super|Shift, -1, XK_x, killunsel, {0} },
	{ Super, -1, XK_c, killclient, {0} },

	{ Super|ControlMask, -1, XK_comma, cyclelayout, {.i = -1 } },
	{ Super|ControlMask, -1, XK_period, cyclelayout, {.i = +1 } },

	{ Super|Shift|ControlMask,	-1, XK_comma, uncyclelayout, {.i = -1 } },
	{ Super|Shift|ControlMask, -1, XK_period, uncyclelayout, {.i = +1 } },
	{ Super|Shift, -1, XK_a, togglefloating, {0} },

	{ Super, -1, XK_grave, view, {.ui = ~0 } },

	{ Super, -1, XK_comma, focusmon, {.i = -1 } },
 	{ Super, -1, XK_period, focusmon, {.i = +1 } },
 	{ Super|Shift, -1, XK_comma, tagmon, {.i = -1 } },
	{ Super|Shift, -1, XK_period, tagmon, {.i = +1 } },

	{ Super|Shift, -1,	XK_f, togglefakefullscreen, {0} },
	{ Super|Shift, -1, XK_e, quit, {.i = 0 } },
	{ Super|Shift, -1, XK_r, quit, {.i = 1 } },

	{ Super|Shift, -1,	XK_g, killmarkkill, {0} },
	{ Super, -1, XK_g, togglemarkkill, {0} },
	{ Super, -1, XK_f, togglefullscr, {0} },
	{ Super|Shift, -1, XK_Print, spawn, SHCMD("flameshot config") },

	{ Super, -1, XK_Down, moveresize, {.v = "0x 25y 0w 0h" } },
	{ Super, -1, XK_Up, moveresize, {.v = "0x -25y 0w 0h" } },
	{ Super, -1, XK_Right, moveresize, {.v = "25x 0y 0w 0h" } },
	{ Super, -1, XK_Left, moveresize, {.v = "-25x 0y 0w 0h" } },

	{ Super|Shift, -1, XK_Down, moveresize, {.v = "0x 0y 0w 25h" } },
	{ Super|Shift, -1, XK_Up, moveresize, {.v = "0x 0y 0w -25h" } },
	{ Super|Shift, -1, XK_Right, moveresize, {.v = "0x 0y 25w 0h" } },
	{ Super|Shift, -1, XK_Left, moveresize, {.v = "0x 0y -25w 0h" } },

	{ Super|ControlMask, -1, XK_Up, moveresizeedge, {.v = "t" } },
	{ Super|ControlMask, -1, XK_Down, moveresizeedge, {.v = "b" } },
	{ Super|ControlMask, -1, XK_Left, moveresizeedge, {.v = "l" } },
	{ Super|ControlMask, -1, XK_Right, moveresizeedge, {.v = "r" } },

	{ Super|ControlMask|Shift, -1, XK_Up, moveresizeedge, {.v = "T" } },
	{ Super|ControlMask|Shift, -1, XK_Down, moveresizeedge, {.v = "B" } },
	{ Super|ControlMask|Shift, -1, XK_Left, moveresizeedge, {.v = "L" } },
	{ Super|ControlMask|Shift, -1, XK_Right, moveresizeedge, {.v = "R" } }, 

	{ Super|Alt, -1, XK_q, moveplace, {.ui = WIN_NW } },
	{ Super|Alt, -1, XK_w, moveplace, {.ui = WIN_N } },
	{ Super|Alt, -1, XK_e, moveplace, {.ui = WIN_NE } },
	{ Super|Alt, -1, XK_a, moveplace, {.ui = WIN_W } },
	{ Super|Alt, -1, XK_s, moveplace, {.ui = WIN_C } }
	,
	{ Super|Alt, -1, XK_d, moveplace, {.ui = WIN_E } },
	{ Super|Alt, -1, XK_z, moveplace, {.ui = WIN_SW } },
	{ Super|Alt, -1, XK_x, moveplace, {.ui = WIN_S } },
	{ Super|Alt, -1, XK_c, moveplace, {.ui = WIN_SE } },
	
	{ Super|Alt|Shift, -1, XK_w, movethrow, {.ui = DIR_N } },
	{ Super|Alt|Shift, -1, XK_a, movethrow, {.ui = DIR_W } },
	
	{ Super|Alt|Shift, -1, XK_s, movethrow, {.ui = DIR_C } },
	{ Super|Alt|Shift, -1, XK_d, movethrow, {.ui = DIR_E } },
	{ Super|Alt|Shift, -1, XK_x, movethrow, {.ui = DIR_S } },

	{ Super|Alt, -1, XK_g, toggleattachdirection, {.i = 0 } },
	{ Super|Alt, -1, XK_h, toggleattachdirection, {.i = 1 } },
	
	{ Super|Alt, -1, XK_j, toggleattachdirection, {.i = 2 } },
	{ Super|Alt, -1, XK_k, toggleattachdirection, {.i = 3 } },
	{ Super|Alt, -1, XK_l, toggleattachdirection, {.i = 4 } },
	{ Super|Alt, -1, XK_semicolon, toggleattachdirection, {.i = 5 } },
	
	{ Cap, -1, XK_h, setcfact, {.f = +0.25 } },
	{ Cap, -1, XK_l, setcfact, {.f = -0.25 } },
	{ Cap, -1, XK_u, setcfact, {.f = 0.00 } },
	{ Cap, -1, XK_s, switchcol, {0} },

	{ Cap, -1, XK_comma, tagswapmon, {.i = +1 } },
	{ Cap, -1, XK_period, tagswapmon, {.i = -1 } },
	
	{ Cap|Shift, -1, XK_j, inplacerotate, {.i = +1 } },
	{ Cap|Shift, -1, XK_k, inplacerotate, {.i = -1 } },
	{ Cap|Shift, -1, XK_h, inplacerotate, {.i = +2 } },
	{ Cap|Shift, -1, XK_l, inplacerotate, {.i = -2 } },

	{ Mouse, -1, XK_d, spawn, SHCMD("xdotool mousemove_relative -- +30 0") },
	{ Mouse, -1, XK_a, spawn, SHCMD("xdotool mousemove_relative -- -30 0") },

	{ Mouse, -1, XK_x, spawn, SHCMD("xdotool mousemove_relative -- 0 +30") },
	{ Mouse, -1, XK_w, spawn, SHCMD("xdotool mousemove_relative -- 0 -30") },
	
	{ Mouse, -1, XK_e, spawn, SHCMD("xdotool mousemove_relative -- +30 -30") },
	{ Mouse, -1, XK_q, spawn, SHCMD("xdotool mousemove_relative -- -30 -30") },

	{ Mouse, -1, XK_z, spawn, SHCMD("xdotool mousemove_relative -- -30 +30") },
	{ Mouse, -1, XK_c, spawn, SHCMD("xdotool mousemove_relative -- +30 +30") },

	{ Mouse, -1, XK_i, spawn, SHCMD("xdotool click 1") },
	{ Mouse, -1, XK_o, spawn, SHCMD("xdotool click 2") },
	{ Mouse, -1, XK_p, spawn, SHCMD("xdotool click 3") },
	{ Mouse, -1, XK_k, spawn, SHCMD("xdotool click 4") },
	{ Mouse, -1, XK_j, spawn, SHCMD("xdotool click 5") },
	{ Mouse, -1, XK_h, spawn, SHCMD("xdotool click 6") },
	{ Mouse, -1, XK_l, spawn, SHCMD("xdotool click 7") },
	
	{ Mouses, -1, XK_d, spawn, SHCMD("xdotool mousemove_relative -- +10 0") },
	{ Mouses, -1, XK_a, spawn, SHCMD("xdotool mousemove_relative -- -10 0") },
	
	{ Mouses, -1, XK_x, spawn, SHCMD("xdotool mousemove_relative -- 0 +10") },
	{ Mouses, -1, XK_w, spawn, SHCMD("xdotool mousemove_relative -- 0 -10") },
	       
	{ Mouses, -1, XK_e, spawn, SHCMD("xdotool mousemove_relative -- +10 -10") },
	{ Mouses, -1, XK_q, spawn, SHCMD("xdotool mousemove_relative -- -10 -10") },
               
	{ Mouses, -1, XK_z, spawn, SHCMD("xdotool mousemove_relative -- -10 +10") },
	{ Mouses, -1, XK_c, spawn, SHCMD("xdotool mousemove_relative -- +10 +10") },
	
	{ Mouses, -1, XK_i, mousemove, {.i= 1 } },
	{ Mouses, -1, XK_o, mousemove, {.i= 2 } },
	{ Mouses, -1, XK_p, mousemove, {.i= 3 } },
	
	{ Cap, -1, XK_z, spawn, SHCMD( "killall -s9 sgtk-grid || sgtk-grid" ) },
	
	TAGKEYS( XK_1, 0)
	TAGKEYS( XK_2, 1)
	TAGKEYS( XK_3, 2)
	TAGKEYS( XK_4, 3)
	TAGKEYS( XK_5, 4)
	TAGKEYS( XK_6, 5)
	TAGKEYS( XK_7, 6)
	TAGKEYS( XK_8, 7)
	TAGKEYS( XK_9, 8)
	TAGKEYS( XK_0, 9)
	TAGKEYS( XK_F1, 10)
	TAGKEYS( XK_F2, 11)
	TAGKEYS( XK_F3, 12)
	TAGKEYS( XK_F4, 13)
	TAGKEYS( XK_F5, 14)
	TAGKEYS( XK_F6, 15)
	TAGKEYS( XK_F7, 16)
	TAGKEYS( XK_F8, 17)
	TAGKEYS( XK_F9, 18)
	TAGKEYS( XK_F10, 19)
	TAGKEYS( XK_F11, 20)
	TAGKEYS( XK_F12, 21)
};

