# bloated dwm(eXkc build)  - bloated dynamic window manager
bloated-dwm-exkc is an extremely blosted  and dynamic window manager for X.


# Installation

Edit config.mk to match your local setup (bloated-dwm-exkc is installed into

the /usr/local namespace by default).

Afterwards enter the following command to build and install bloated-dwm-exkc :

````
$ su -c ' make clean install'
````

# Running bloated-dwm-exkc
Add the following line to your .xinitrc to start dwm using startx:

bloated-dwm-exkc

or start bloated-dwm-exkc from display manager

# Status bar

I use xfce4-panel as  status bar. 



