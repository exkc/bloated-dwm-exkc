# bloated-dwm-exkc - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c bloated-dwm-exkc.c util.c
OBJ = ${SRC:.c=.o}

all: options bloated-dwm-exkc

options:
	@echo bloated-dwm-exkc build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

bloated-dwm-exkc: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS} 
clean:
	rm -f bloated-dwm-exkc ${OBJ} bloated-dwm-exkc-${VERSION}.tar.gz
	rm -rf bloated-dwm-exkc-${VERSION}
	rm -rf *.orig
	rm -rf *.rej

dist: clean
	mkdir -p bloated-dwm-exkc-${VERSION}
	cp -R LICENSE Makefile README config.def.h config.mk\
		bloated-dwm-exkc.1 drw.h util.h ${SRC} bloated-dwm-exkc.png transient.c bloated-dwm-exkc-${VERSION}
	tar -cf bloated-dwm-exkc-${VERSION}.tar bloated-dwm-exkc-${VERSION}
	gzip bloated-dwm-exkc-${VERSION}.tar
	rm -rf bloated-dwm-exkc-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f bloated-dwm-exkc ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/bloated-dwm-exkc
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < bloated-dwm-exkc.1 > ${DESTDIR}${MANPREFIX}/man1/bloated-dwm-exkc.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/bloated-dwm-exkc.1
	cp -f  ./script/* ${DESTDIR}${PREFIX}/bin
	chmod +x  ${DESTDIR}${PREFIX}/bin/*
	mkdir -p /usr/share/xsessions/	
	cp bloated-dwm-eXkc.desktop  /usr/share/xsessions
uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/bloated-dwm-exkc\
		${DESTDIR}${MANPREFIX}/man1/bloated-dwm-exkc.1

.PHONY: all options clean dist install uninstall
